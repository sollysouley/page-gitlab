FROM node:12-alpine
RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app/node_modules
WORKDIR /home/node/app
COPY ./package*.json ./
USER node
RUN npm install
COPY --chown=node:node . .
EXPOSE 8080
RUN ./import.sh
RUN ./update-config.sh
CMD ["npm","run","docs:dev"]

