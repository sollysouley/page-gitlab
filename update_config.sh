ls -d ./docs/*/ > input1
while read in
do
	echo "'${in:6}': [ ''" >> out
	ls -I README.md -A $in/ >> input2
	while read in2
	do
		echo ",'$in2'" >> out
	done < input2
	echo "],">> out
	rm input2
done < input1
rm input1

cat ./docs/.template/configjs/config_up > ./docs/.vuepress/config.js
cat out >> ./docs/.vuepress/config.js
cat ./docs/.template/configjs/config_down >> ./docs/.vuepress/config.js
rm out
