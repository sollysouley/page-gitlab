---
home: true
heroImage: /images/hero.png
heroText: 3D - DGDDI, Data, Documentation
actionText: Consulter la documentation →
actionLink: /accueil
tagline: toute la documentation
features:
- title: Pour qui ?
  details: Cette plateforme de documentation technique et fonctionnelle est destinée à l'ensemble des équipes travaillant sur des cas d'usage data hébergés sur la plateforme de data mining.
- title: Sont invités à la consulter
  details: Les métiers, MOA et AMOA participant aux CU, les pôles développement et fonctionnement de la DNSCE ainsi que les pôle data et système impliqués dans la gestion et la maintenance de la plateforme DTM.
- title: Hébergement par DTM 
  details: Cette plateforme est hébergée et administrée collaborativement par l'équipe de DataMining. Elle est une implémentation du logiciel Gitlab, installé sur la plateforme.

footer: MIT Licensed | Copyright © 2018-present Evan You
---
