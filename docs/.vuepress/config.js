module.exports = {
	title: 'site vp',
	description: 'demo vp',
	base: '/',
	dest: 'public',
	themeConfig: {
		logo: '/images/logo.jpg',
		theme: 'vuepress-theme-gouv-fr',
		nav: [
			{ text: 'Home', link: '/'},
			{ text: 'Tout les guides', link: '/accueil'},
			{ text: 'Tutoriel Gitlab', link: '/tuto'}
		],
		displayAllHeaders: true,

		sidebarDepth: 2,
		sidebar: {



'/poc_cu_operation-douaniere_operateur/': [ ''
,'operation-douaniere_back.md'
,'operation-douaniere_front.md'
,'operation-douaniere_preprocessing.md'
],
'/pocpoc/': [ ''
,'file1'
,'file2'
],
	    'baz' : 'auto',
            //fallback
            '/':['']
        }
    }
};
