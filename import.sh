while read in
do
	git clone $in

	C=$(dirname "${in}")
	GrpCU=$( echo $C |tr "/" " "| grep -oE '[^ ]+$')
	CU=$(basename "${in%.*}")
	
	##echo "C = $C" >> ouput
	##echo "GrpCU= $GrpCU" >> ouput
	##echo "CU = $CU" >> ouput
	
	# creation des fichiers /docs/grpcu/CU.md
	mkdir -p ./docs/$GrpCU
	
	echo "# $GrpCU" > ./docs/$GrpCU/README.md
	cat ./$CU/documentation/*.md > ./docs/$GrpCU/$CU.md
	
	# copie des images 
	cp ./$CU/documentation/images/*  ./docs/$GrpCU/images/
	
	# mise à jour du fichier /docs/tmp_accueil.md afin de générer le fichier ./docs/accueil.md 
	echo  "<a href=\"/$GrpCU/\" class=\"nav-link action-button\">$GrpCU</a>" >> ./docs/.template/accueil/tmp_accueil

	#Mise à jour du ficher /docs/.vuepress.md/tmp_config afin de généré le fichier  config.js.js
	echo  "'/$GrpCU/' : ['','$CU']," >> ./output

	

	
	
	rm -rf ./$CU/
done < list-CU.txt

#génération du fichier ./docs/acceuil.md
cat ./docs/.template/accueil/tmplt_accueil_up > ./docs/.template/accueil/acc
cat ./docs/.template/accueil/tmp_accueil >> ./docs/.template/accueil/acc
cat ./docs/.template/accueil/tmplt_accueil_dwn >> ./docs/.template/accueil/acc
uniq ./docs/.template/accueil/acc ./docs/accueil.md
rm ./docs/.template/accueil/tmp_accueil ./docs/.template/accueil/acc
#génération du fichier ./docs/.vuepress/config.
#cat ./docs/template/accueil/tmplt_accueil_up > ./docs/template/accueil/acc
#cat ./docs/template/accueil/tmp_accueil >> ./docs/template/accueil/acc
#cat ./docs/template/accueil/tmplt_accueil_dwn >> ./docs/template/accueil/acc
#uniq ./docs/template/accueil/acc ./docs/accueil.md
#rm ./docs/template/accueil/tmp_accueil ./docs/template/accueil/acc
